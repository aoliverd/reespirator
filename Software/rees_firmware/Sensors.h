

/*
  SFM3300.cpp - Library for reading values from flowmeter Sensirion SFM3300

  Created by WeDo, Zuerich 20170616
  Modified 20200322
  Released into the public domain.  
*/

#ifndef Sensors_h
#define Sensors_h
 
#if ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
  #include "pins_arduino.h"
  #include "WConstants.h"
#endif
 

class SFM3300 {
  public:
	SFM3300(int i2cAddress);
    void init();
    float getvalue();
    
 
  private:
	//uint8_t mI2cAddress;
	int mI2cAddress;
	uint8_t crc8(const uint8_t data, uint8_t crc);
};
   
#endif
